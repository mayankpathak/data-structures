from Lists.Exceptions import *
from Lists.Nodes import SinglyNode


class List(object):
    """
    Class representing a Circular Linked List
    """
    def __init__(self):
        self.__head = None
        self.__tail = None
        self.__length = 0
        self.__errors = {
            "position": "Invalid Position for insertion.",
            "empty": "List is empty.",
            "node": "Node is not in the list.",
            "value": "Value is not in the list."
        }

    # Get the length of the Circular Linked List
    def __len__(self):
        return self.__length

    # Traverse the Circular Linked List
    def traverse(self):
        if self.__head is None:
            raise ListEmptyError(self.__errors["empty"])
        else:
            temp = self.__head
            print(temp.get_data())
            temp = temp.get_next()
            while temp is not self.__head:
                print(temp.get_data())
                temp = temp.get_next()

    # Insert at the beginning of a Circular Linked List
    def pre_pend(self, value):
        new_node = SinglyNode()
        new_node.set_data(value)
        if self.__head is None:
            self.__head = new_node
            self.__tail = new_node
        else:
            new_node.set_next(self.__head)
            self.__head = new_node
        self.__tail.set_next(self.__head)
        self.__length += 1

    # Insert at the end of a Circular Linked List
    def append(self, value):
        new_node = SinglyNode()
        new_node.set_data(value)
        if self.__head is None:
            self.__head = new_node
            self.__tail = new_node
        else:
            self.__tail.set_next(new_node)
            self.__tail = new_node
        self.__tail.set_next(self.__head)
        self.__length += 1

    # Insert at the given position in a Circular Linked List
    def insert(self, value, position):
        if position > self.__length or position < 0:
            raise PositionError(self.__errors["position"])
        elif position == 0:
            self.pre_pend(value)
        elif position == self.__length:
            self.append(value)
        else:
            new_node = SinglyNode()
            new_node.set_data(value)
            temp = self.__head
            count = 0
            while count < position - 1:
                temp = temp.get_next()
                count += 1
            new_node.set_next(temp.get_next())
            temp.set_next(new_node)
            self.__length += 1

    # Delete from the beginning of the Circular Linked List
    def delete_beginning(self):
        if self.__length == 0:
            raise ListEmptyError(self.__errors["empty"])
        else:
            self.__head = self.__head.get_next()
            self.__tail.set_next(self.__head)
            self.__length -= 1

    # Delete from the end of the Circular Linked List
    def delete_last(self):
        if self.__length == 0:
            raise ListEmptyError(self.__errors["empty"])
        elif self.__length == 1:
            self.__head = None
            self.__tail = None
        else:
            current = self.__head
            while current.get_next() != self.__tail:
                current = current.get_next()
            current.set_next(self.__head)
            self.__tail = current
            self.__length -= 1

    # Delete from the given position in the Circular Linked List
    def delete(self, position):
        if position > self.__length or position < 0:
            raise PositionError(self.__errors["position"])
        elif position == 0:
            self.delete_beginning()
        elif position == self.__length:
            self.delete_last()
        else:
            current = self.__head
            previous = self.__head
            count = 0
            while count < position:
                count += 1
                previous = current
                current = current.get_next()
            previous.set_next(current.get_next())
            self.__length -= 1

    # Delete the entire Singly Linked List
    def clear(self):
        self.__head = None
        self.__tail = None
        self.__length = 0
