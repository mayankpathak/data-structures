class PositionError(Exception):
    """
    Exception to be raised when the position to be inserted/deleted is
    either negative or greater than the length of the list
    """
    def __init__(self, value):
        self.__error_message = value

    def __str__(self):
        return repr(self.__error_message)
