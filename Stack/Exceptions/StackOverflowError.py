class StackOverflowError(Exception):
    """
    Exception to be raised when the Stack is full and a request is made for insertion.
    """
    def __init__(self, value):
        self.__error_message = value

    def __str__(self):
        return self.__error_message
