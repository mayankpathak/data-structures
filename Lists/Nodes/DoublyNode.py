from Lists.Nodes.SinglyNode import SinglyNode


class DoublyNode(SinglyNode):
    """
    Class representing the node of a Doubly Linked List.
    This node inherits from the SinglyNode.
    Each Node has a data a next field and a previous field.
    """
    def __init__(self):
        super(DoublyNode, self).__init__()
        self.__previous = None

    # Set previous of a node to given node/None
    def set_previous(self, value):
        self.__previous = value

    # Get previous of a node
    def get_previous(self):
        return self.__previous

    # To check if the node has previous pointing to another node
    def has_previous(self):
        return self.__previous is not None
