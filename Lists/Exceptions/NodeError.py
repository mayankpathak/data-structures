class NodeError(Exception):
    """
    Exception to be raised when the Node to be deleted is not present in the list.
    """
    def __init__(self, value):
        self.__error_message = value

    def __str__(self):
        return repr(self.__error_message)
