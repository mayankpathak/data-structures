from Stack.Exceptions import *


class Stack(object):
    """
    Class representing a Stack implemented using a static array
    """
    def __init__(self, value):
        self.__memory = [] * value
        self.__capacity = value
        self.__size = 0
        self.__errors = {
            "overflow": "Stack is full.",
            "underflow": "Stack is empty.",
        }

    # Get the length of the Stack
    def __len__(self):
        return self.__size

    # Get the capacity of the Stack
    def get_capacity(self):
        return self.__capacity

    # Traverses the Stack from first element to the last
    def traverse(self):
        for i in range(self.__size):
            print(self.__memory[i])

    # To find whether the Stack is empty or not
    def is_empty(self):
        return self.__size == 0

    # To find whether the Stack is full or not
    def is_full(self):
        return self.__size == self.__capacity

    # Pushes an element on top of Stack if the Stack is not full.
    # Raises an exception otherwise.
    def push(self, value):
        if self.is_full():
            raise StackOverflowError(self.__errors["overflow"])
        else:
            self.__memory.append(value)
            self.__size += 1

    # Removes the top element of a Stack and returns it.
    # Raises an exception if the Stack is empty.
    def pop(self):
        if self.is_empty():
            raise StackUnderflowError(self.__errors["underflow"])
        else:
            self.__size -= 1
            return self.__memory.pop()

    # Returns the top element of a Stack without removing it.
    # Raises an exception if the Stack is empty.
    def peek(self):
        if self.is_empty():
            raise StackUnderflowError(self.__errors["underflow"])
        else:
            return self.__memory[-1]

    # Clears the content of the Stack
    def clear(self):
        self.__memory = [] * self.__capacity
        self.__size = 0
