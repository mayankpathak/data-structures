from Queue.Exceptions import *
from Lists.Nodes.DoublyNode import *


class Queue(object):
    """
    Class representing a Queue implemented using a Doubly Linked List
    """
    def __init__(self):
        self.__front = None
        self.__rear = None
        self.__size = 0
        self.__errors = {
            "underflow": "Queue is empty.",
        }

    # Get the length of the Queue
    def __len__(self):
        return self.__size

    # Get the rear of the Queue
    def get_rear(self):
        if self.is_empty():
            raise QueueUnderflowError(self.__errors["underflow"])
        else:
            return self.__rear.get_data()

    # Get the front of the Queue
    def get_front(self):
        if self.is_empty():
            raise QueueUnderflowError(self.__errors["underflow"])
        else:
            return self.__front.get_data()

    # Traverses the Queue from first element to the last
    def traverse(self):
        if self.is_empty():
            raise QueueUnderflowError(self.__errors["underflow"])
        else:
            temp = self.__front
            while temp is not None:
                print(temp.get_data())
                temp = temp.get_next()

    # To find whether the Queue is empty or not
    def is_empty(self):
        return self.__size == 0

    # Places an element on rear of Queue
    def enqueue(self, value):
        temp = DoublyNode()
        temp.set_data(value)
        if self.is_empty():
            self.__front = temp
            self.__rear = temp
        else:
            self.__rear.set_next(temp)
            temp.set_previous(self.__rear)
            self.__rear = temp
        self.__size += 1

    # Removes the top element of a Queue and returns it.
    # Raises an exception if the Queue is empty
    def dequeue(self):
        if self.is_empty():
            raise QueueUnderflowError(self.__errors["underflow"])
        else:
            self.__size -= 1
            result = self.__front.get_data()
            self.__front = self.__front.get_next()
            self.__front.set_previous(None)
            return result

    # Clears the content of the Stack
    def clear(self):
        self.__front = None
        self.__rear = None
        self.__size = 0
