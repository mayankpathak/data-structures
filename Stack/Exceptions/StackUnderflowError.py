class StackUnderflowError(Exception):
    """
    Exception to be raised when the Stack is empty and a request is made for deletion.
    """
    def __init__(self, value):
        self.__error_message = value

    def __str__(self):
        return self.__error_message
