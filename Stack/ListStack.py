from Stack.Exceptions import *
from Lists.Nodes.SinglyNode import *


class Stack(object):
    """
    Class representing a Stack implemented using a Linked List
    """
    def __init__(self, value):
        self.__head = None
        self.__size = 0
        self.__errors = {
            "underflow": "Stack is empty.",
        }
        if value:
            for _ in value:
                self.push(_)

    # Get the length of the Stack
    def __len__(self):
        return self.__size

    # Traverses the Stack from first element to the last
    def traverse(self):
        if self.is_empty():
            raise StackUnderflowError(self.__errors["underflow"])
        else:
            temp = self.__head
            while temp is not None:
                print(temp.get_data())
                temp = temp.get_next()

    # To find whether the Stack is empty or not
    def is_empty(self):
        return self.__size == 0

    # Pushes an element on top of Stack.
    def push(self, value):
        temp = SinglyNode()
        temp.set_data(value)
        temp.set_next(self.__head)
        self.__head = temp
        self.__size += 1

    # Removes the top element of a Stack and returns it.
    # Raises an exception if the Stack is empty
    def pop(self):
        if self.is_empty():
            raise StackUnderflowError(self.__errors["underflow"])
        else:
            self.__size -= 1
            result = self.__head.get_data()
            self.__head = self.__head.get_next()
            return result

    # Returns the top element of a Stack without removing it.
    # Raises an exception if the Stack is empty
    def peek(self):
        if self.is_empty():
            raise StackUnderflowError(self.__errors["underflow"])
        else:
            return self.__head.get_data()

    # Clears the content of the Stack
    def clear(self):
        self.__head = None
        self.__size = 0
