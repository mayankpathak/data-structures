from Queue.Exceptions import *


class Queue(object):
    """
    Class representing a Queue implemented using a static array
    """
    def __init__(self, value):
        self.__memory = [] * value
        self.__capacity = value
        self.__size = 0
        self.__front = None
        self.__rear = None
        self.__errors = {
            "overflow": "Queue is full.",
            "underflow": "Queue is empty.",
        }

    # Get the length of the Queue
    def __len__(self):
        return self.__size

    # Get the capacity of the Queue
    def get_capacity(self):
        return self.__capacity

    # Get the rear of the Queue
    def get_rear(self):
        if self.is_empty():
            raise QueueUnderflowError(self.__errors["underflow"])
        else:
            return self.__memory[self.__rear]

    # Get the front of the Queue
    def get_front(self):
        if self.is_empty():
            raise QueueUnderflowError(self.__errors["underflow"])
        else:
            return self.__memory[self.__front]

    # Traverses the Queue from front to rear
    def traverse(self):
        if self.is_empty():
            raise QueueUnderflowError(self.__errors["underflow"])
        else:
            index = self.__front
            while index != self.__rear:
                print(self.__memory[index])
                index += 1
                if index >= self.__size:
                    index = 0
            print(self.__memory[index])

    # To find whether the Queue is empty or not
    def is_empty(self):
        return self.__size == 0

    # To find whether the Queue is full or not
    def is_full(self):
        return self.__size == self.__capacity

    # Places an element on rear of Queue if the Queue is not full.
    # Raises an exception otherwise.
    def enqueue(self, value):
        if self.is_full():
            raise QueueOverflowError(self.__errors["overflow"])
        else:
            self.__memory.append(value)
            if self.__front is None:
                self.__front = 0
                self.__rear = 0
            else:
                self.__rear = self.__size
            self.__size += 1

    # To find whether the Queue is full or not
    def dequeue(self):
        if self.is_empty():
            raise QueueUnderflowError(self.__errors["underflow"])
        else:
            result = self.__memory.pop(0)
            self.__size -= 1
            if self.is_empty():
                self.__front = None
                self.__rear = None
            else:
                self.__rear = self.__size - 1
            return result

    # Clears the content of the Queue
    def clear(self):
        self.__memory = [] * self.__capacity
        self.__size = 0
