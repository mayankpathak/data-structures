class SinglyNode:
    """
    Class representing the node of a Singly Linked List.
    Each Node has a data and a next field.
    """
    def __init__(self):
        self.__data = None
        self.__next = None

    def __str__(self):
        return "SinglyNode -> Data = %s" % str(self.__data)

    # Set data of a node to given value
    def set_data(self, value):
        self.__data = value

    # Get data of a node
    def get_data(self):
        return self.__data

    # Set next of a node to given node/None
    def set_next(self, value):
        self.__next = value

    # Get next of a node
    def get_next(self):
        return self.__next

    # To check if the node has next pointing to another node
    def has_next(self):
        return self.__next is not None
