from Lists.Exceptions import *
from Lists.Nodes import DoublyNode


class List(object):
    """
    Class representing a Doubly Linked List
    """
    def __init__(self):
        self.__head = None
        self.__tail = None
        self.__length = 0
        self.__errors = {
            "position": "Invalid Position for insertion.",
            "empty": "List is empty.",
            "node": "Node is not in the list.",
            "value": "Value is not in the list."
        }

    # Get the length of the Doubly Linked List
    def __len__(self):
        return self.__length

    # Traverse the Doubly Linked List in either direction
    def traverse(self, reverse=False):
        if self.__head is None:
            raise ListEmptyError(self.__errors["empty"])
        elif reverse:
            temp = self.__tail
            while temp is not None:
                print(temp.get_data())
                temp = temp.get_previous()
        else:
            temp = self.__head
            while temp is not None:
                print(temp.get_data())
                temp = temp.get_next()

    # Insert at the beginning of a Doubly Linked List
    def pre_pend(self, value):
        new_node = DoublyNode()
        new_node.set_data(value)
        if self.__head is None:
            self.__head = new_node
            self.__tail = new_node
        else:
            new_node.set_next(self.__head)
            self.__head.set_previous(new_node)
            self.__head = new_node
        self.__length += 1

    # Insert at the end of a Doubly Linked List
    def append(self, value):
        new_node = DoublyNode()
        new_node.set_data(value)
        if self.__head is None:
            self.__head = new_node
            self.__tail = new_node
        else:
            self.__tail.set_next(new_node)
            new_node.set_previous(self.__tail)
            self.__tail = new_node
        self.__length += 1

    # Insert at the given position in a Doubly Linked List
    def insert(self, value, position):
        if position > self.__length or position < 0:
            raise PositionError(self.__errors["position"])
        elif position == 0:
            self.pre_pend(value)
        elif position == self.__length:
            self.append(value)
        else:
            new_node = DoublyNode()
            new_node.set_data(value)
            temp = self.__head
            count = 0
            while count < position - 1:
                temp = temp.get_next()
                count += 1
            new_node.set_next(temp.get_next())
            new_node.get_next().set_previous(new_node)
            temp.set_next(new_node)
            new_node.set_previous(temp)
            self.__length += 1

    # Delete from the beginning of the Doubly Linked List
    def delete_beginning(self):
        if self.__length == 0:
            raise ListEmptyError(self.__errors["empty"])
        else:
            self.__head = self.__head.get_next()
            self.__head.set_previous(None)
            self.__length -= 1

    # Delete from the end of the Doubly Linked List
    def delete_last(self):
        if self.__length == 0:
            raise ListEmptyError(self.__errors["empty"])
        elif self.__length == 1:
            self.__head = None
            self.__tail = None
        else:
            self.__tail.get_previous().set_next(None)
            self.__length -= 1

    # Delete from the given position in the Doubly Linked List
    def delete(self, position):
        if position > self.__length or position < 0:
            raise PositionError(self.__errors["position"])
        elif position == 0:
            self.delete_beginning()
        elif position == self.__length:
            self.delete_last()
        else:
            current = self.__head
            previous = self.__head
            count = 0
            while count < position:
                count += 1
                previous = current
                current = current.get_next()
            previous.set_next(current.get_next())
            previous.get_next().set_previous(previous)
            self.__length -= 1

    # Delete the entire Doubly Linked List
    def clear(self):
        self.__head = None
        self.__tail = None
        self.__length = 0
